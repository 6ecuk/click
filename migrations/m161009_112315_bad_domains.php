<?php

use yii\db\Migration;

class m161009_112315_bad_domains extends Migration
{
    const TABLE_NAME = 'bad_domains';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
        ]);

        $this->createIndex('unique_name', self::TABLE_NAME, 'name', true);
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
