<?php

use yii\db\Migration;

class m161009_112250_click extends Migration
{
    const TABLE_NAME = 'click';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->string(255)->notNull(),
            'ua' => $this->string(255)->notNull(),
            'ip' => $this->integer()->unsigned(),
            'ref'=> $this->string(255),
            'param1' => $this->string(100),
            'param2' => $this->string(100),
            'error'  => $this->integer()->unsigned(),
            'bad_domain' => $this->smallInteger()->defaultValue(0)->notNull()
        ]);

        $this->addPrimaryKey('pk_id', self::TABLE_NAME, 'id');
    }

    public function down()
    {
         $this->dropTable(self::TABLE_NAME);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
