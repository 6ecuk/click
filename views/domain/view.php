<?php
/**
 * Created by PhpStorm.
 * User: EvgeniyK
 * Date: 09.10.16
 * Time: 17:13
 */


use yii\helpers\Html;

$this->title = 'Bad domains list';
?>
<div>
    <?php echo Html::a('Add domain', 'bad-domains/add') ?>
</div>
<div class="site-error">
    <ul>
        <?php
        foreach ($badDomains as $domain) {
            echo Html::beginTag('li');
            echo $domain->name;
            echo Html::endTag('li');
        }
        ?>
    </ul>
</div>