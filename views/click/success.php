<?php
use yii\helpers\Html;
/**
 * Created by PhpStorm.
 * User: EvgeniyK
 * Date: 09.10.16
 * Time: 17:13
 */

$this->title = 'Error: ' . $successData->id;
?>

<div class="site-error">

    <h1>Congratulations, your click  with id <?php echo Html::encode($successData->id)?> was counted</h1>
    <p> Your IP: <?php echo long2ip($successData->ip); ?></p>
    <p> Your User Agent: <?php echo $successData->ua; ?></p>
</div>