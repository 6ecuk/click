<?php
use yii\helpers\Html;
/**
 * Created by PhpStorm.
 * User: EvgeniyK
 * Date: 09.10.16
 * Time: 17:13
 */

$this->title = 'Error: ' . $errorData->id;
if ($isBadDomain) {
    $this->registerJs('
        $(document).ready(function(){
            setTimeout(function(){
            window.location="http://www.google.com"
            }, 5000);
        });
    ');
}
?>
<script></script>
<div class="site-error">
 <?php
 if(!$isBadDomain)
 {
     echo Html::beginTag('h1') . 'Sorry, but click  with id ' .  Html::encode($errorData->id) .' not counted' . Html::endTag('h1');
 }
 else {
     echo Html::beginTag('h1') . 'Sorry, but domain in block list  ' . Html::endTag('h1');
 }
 ?>

<p>
    The above error occurred while the click server was processing your request.
</p>
<p>
    Please contact us if you think this is a mistake. Thank you.
</p>

</div>
