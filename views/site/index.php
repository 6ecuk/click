<?php
/* @var yii\web\View $this */
use fedemotta\datatables\DataTables;

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <?= DataTables::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'ipView',
            'ua',
            'ref',
        ],
    ]);?>
</div>
