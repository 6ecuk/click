<?php
/**
 * Created by PhpStorm.
 * User: EvgeniyK
 * Date: 09.10.16
 * Time: 15:19
 */

namespace app\controllers;

use app\models\BadDomains;
use app\models\Click;
use Yii;
use yii\web\Controller;

class ClickController extends Controller
{
    /**
     * @param null $param1
     * @param null $param2
     */
    public function actionIndex($param1 = null, $param2 = null)
    {
        //TODO::implement validators for parameters
        $domain =  parse_url(Yii::$app->request->getReferrer(), PHP_URL_HOST);
        $domainIsValid = $this->domainIsValid($domain);
        $id = md5(
            Yii::$app->request->getUserIP() .
            Yii::$app->request->getUserAgent() .
            $domain .
            $param1
        );
        $clickModel = Click::find()->where(['id' => $id])->one();
        if ($clickModel){
            $clickModel->error++;
            $clickModel->bad_domain = $domainIsValid ? 0 : 1;
            $clickModel->save();
            $this->redirect(['error',  'id' => $clickModel->id, 'isBadDomain' => $domainIsValid ? false : true]);
        }
        else
            {
                $clickModel = new Click();
                $clickModel->id = $id;
                $clickModel->ip = Yii::$app->request->getUserIP()?ip2long(Yii::$app->request->getUserIP()):null;
                $clickModel->ua = Yii::$app->request->getUserAgent()?:'';
                $clickModel->ref = $domain;
                $clickModel->param1 = $param1;
                $clickModel->param2 = $param2;
                $clickModel->bad_domain = $domainIsValid ? 0 : 1;
                $clickModel->save();
                $this->redirect(['success',  'id' => $clickModel->id]);
            }
    }

    /**
     * @param $id
     * @return string
     */
    public function actionSuccess($id)
    {
        $clickData = Click::find()->where(['id' => $id])->one();
        return $this->render('success', ['successData'=> $clickData]);
    }

    /**
     * @param $id
     * @param bool $isBadDomain
     * @return string
     */
    public function actionError($id, $isBadDomain = false)
    {
        $clickData = Click::find()->where(['id' => $id])->one();
        if($clickData === null)
        {
            return $this->actionNotFound();
        }
        return $this->render('error', ['errorData'=> $clickData, 'isBadDomain' => $isBadDomain]);
    }

    /**
     * @return string
     */
    public function actionNotFound()
    {
        return $this->render('/site/error', ['name'=> 'Not Found', 'message' => 'Sorry, id is not correct']);
    }

    /**
     * @param $domain
     * @return bool
     */
    public function domainIsValid($domain)
    {
        return !empty($domain) ? count(BadDomains::find()->where(['name' => $domain])->all()) === 0 : false;
    }
}