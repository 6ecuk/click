<?php
/**
 * Created by PhpStorm.
 * User: EvgeniyK
 * Date: 09.10.16
 * Time: 20:53
 */

namespace app\controllers;

use app\models\BadDomains;
use Yii;
use yii\web\Controller;

class DomainController extends Controller
{

    /**
     * @return string
     */
    public function actionIndex()
    {
        $badDomainsModel = BadDomains::find()->orderBy(['id' => SORT_ASC])->all();
        return $this->render('view', ['badDomains' => $badDomainsModel]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionAdd()
    {
        $model = new BadDomains();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('add', ['model' => $model]);
        }
    }

}