<?php
/**
 * Created by PhpStorm.
 * User: becuk
 * Date: 10.10.16
 * Time: 2:16
 */

namespace app\models;


use yii\data\ActiveDataProvider;

class ClickSearch extends Click
{

    public function search()
    {
        $query = ClickSearch::find()->select(['id','ua', 'ref', 'INET_NTOA(ip) as ipView']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }
}