<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "click".
 *
 * @property string $id
 * @property string $ua
 * @property integer $ip
 * @property string $ref
 * @property string $param1
 * @property string $param2
 * @property integer $error
 * @property integer $bad_domain
 */
class Click extends \yii\db\ActiveRecord
{
     public $ipView;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'click';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'ua'], 'required'],
            [['ip', 'error', 'bad_domain'], 'integer'],
            [['id', 'ua', 'ref'], 'string', 'max' => 255],
            [['param1', 'param2'], 'string', 'max' => 100],
            ['ipView', 'unsafe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ua' => 'Ua',
            'ip' => 'Ip',
            'ipView' => 'Ip',
            'ref' => 'Ref',
            'param1' => 'Param1',
            'param2' => 'Param2',
            'error' => 'Error',
            'bad_domain' => 'Bad Domain',
        ];
    }
}
